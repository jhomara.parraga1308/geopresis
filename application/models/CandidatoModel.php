<?php

class CandidatoModel extends CI_Model
{

  function __construct()
  {
    parent::__contruct();
  }

  function obtenerTodos(){
    $listadoCandidatos=$this->db->get('candidato');
    if($listadoCandidatos->num_rows()>0){
      return $listadoCandidatos->result();
    }
    return false;
  }

  function obtenerTodosPresidentes(){

    $listadoPresidente=$this->db->select()->where('dignidad_can','Presidente')->get("candidato");
    if ($listadoPresidente->num_rows()>0)
      {
        return $listadoPresidente->result();
      }
      return false;
  }
  function obtenerTodosAsambleistas(){
    $listadoPresidente=$this->db->select()->where('dignidad_can','Asambleista Nacional')->get("candidato");
    if ($listadoPresidente->num_rows()>0)
    {
      return $listadoPresidente->result();
    }
    return false;
  }
  function obtenerTodosAsambleistasPro(){
    $listadoPresidente=$this->db->select()->where('dignidad_can','Asambleista Provincial')->get("candidato");
    if ($listadoPresidente->num_rows()>0)
    {
      return $listadoPresidente->result();
    }
    return false;
  }
} //fin de la class

 ?>
